cp = 1
cr = 0
armorBonusActive = false

use_next = {
    0: "'Outmaneuver', then 'Weigh the Odds' (early) or Attack (late)",
    1: "(-1) 'Rally Now', then 'Outmaneuver' and 'Weigh the Odds' | attack if just used 'You Set Them Up...'",
    2: "(-2) 'Try Again' asap to get 0",
    3: "don't use anything, 'Weigh the Odds' next turn",
    4: "(-4) 'You Set Them Up, I Finish'"
}

document.getElementById("cpTracker").innerHTML = cp
document.getElementById("crTracker").innerHTML = cr
document.getElementById("armorBonus").innerHTML = armorBonusActive ? "+2" : "OFF"
setNextMove()


function setNextMove() {
    targetIndex = cp > 4 ? 4 : cp
    nextMove = use_next[targetIndex]
    document.getElementById("nextMove").innerHTML = nextMove
}

function commandPoints(amount) {
    cp += amount
    if (cp < 0) {
        cp = 0
    }
    document.getElementById("cpTracker").innerHTML = cp
    setNextMove()
}

function resetCommandPoints() {
    cp = 0
    document.getElementById("cpTracker").innerHTML = cp
    setNextMove()
}

function critRange(amount) {
    cr += amount
    if (cr < 0) {
        cr = 0
    }
    document.getElementById("crTracker").innerHTML = cr
}

function resetCritRange() {
    cr = 0
    document.getElementById("crTracker").innerHTML = cr
}

function toggleArmorBonus() {
    armorBonusActive = !armorBonusActive
    document.getElementById("armorBonus").innerHTML = armorBonusActive ? "+2" : "OFF"
}
