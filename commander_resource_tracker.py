cp = 0
cr = 0

use_next = {
    0: "'Outmaneuver', then 'Weigh the Odds' (early) or Attack (late)",
    1: "'Weigh the Odds', unless just used 'You Set Them Up...', then Attack",
    2: "(-2) 'Try Again' asap to get 0",
    3: "don't use anything, 'Weigh the Odds' next turn",
    4: "(-4) 'You Set Them Up, I Finish'"
}

def adjust_cp(amount):
    global cp
    cp = cp + int(amount)
    print(f'\nuse: {use_next[cp]}')

def adjust_cr(amount):
    global cr
    cr = cr + int(amount)

commands = {
    'cp': adjust_cp,
    'cr': adjust_cr
}

def print_stats():
    print(f"\nCP: {cp}")
    print(f"CR: {cr}\n")

while 1:
    print(f'available commands: {", ".join([*commands.keys()])}')
    user_input = input('Choose your path: ').strip().replace('\x1b[A', '').replace('\x1b[B', '')
    split_input = user_input.split(' ')
    try:
        operation = commands[split_input[0]]
        if (len(split_input)) == 2:
            operation(split_input[1])
        else:
            print(f'wrong number of params; expected format "command amount", got "{user_input}"')
    except KeyError:
        print(f'{split_input[0]} is an invalid command')

    print_stats()
