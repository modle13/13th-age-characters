---
title: "Forgeborn Commander"
date: 2020-06-02T12:00:00-06:00
draft: false
url: /13th-age-commander
tags:
- Gaming
- 13th Age Characters
- d20s
---

# Amalgam the Discarded (Forgeborn Commander 1)

## build

- [game tool](http://kalafina.incorporeal.org:30000/game)
- [worksheet](https://docs.google.com/spreadsheets/d/1zkhhLSgQxyc8VKAHZiM4GNr25H38W2gvt1VYM3aodik/edit?usp=sharing)
- [commander SRD](https://www.13thagesrd.com/classes/commander/)
- [fighter SRD](https://www.13thagesrd.com/classes/fighter/)


### ability scores

Is CHA a handicap? Do I care too much about STR here?

|             | STR               | CON | DEX | INT | WIS | CHA               |
|-------------|-------------------|-----|-----|-----|-----|-------------------|
| score       | 17 (Forgeborn +2) | 15  | 12  | 8   | 8   | 17 (commander +2) |
| mod + level | +4                | +3  | +2  | +0  | +0  | +4                |

### one unique thing

Rejected by its creators as a failed prototype and abandoned in a scrap pit, Amalgam had to finish the job its masters deemed unworthy of finishing by collecting and fusing with various other cast-off parts. As such, it is skilled at assimilating useful components it comes across.

### icon relationships

- Conflicted - Dwarf King (2)
- Positive - The Emperor (1)

### backgrounds

- (4) puzzle master: it basically made itself; it knows how things work, how they fit together, and how they can be undone
- (2) emergency sawbones: its detailed understanding of anatomy and structures makes it a deft hand at tending wounds--and cutting losses when necessary
- (2) master brewer: hold on, it has an attachment for that somehwere...

### health, initiative

| Hit Points | Recoveries | Recovery Dice | Initiative |
|---|---|---|---|
| 36 | 8 | 1d8+2 | +2 |

### defenses

| AC 17                                                                           | PD 13 | MD 12 |
|---------------------------------------------------------------------------------|----|----|
| 14 base (Heavy Armor)<br>1 from Shield<br>1 from ability scores<br>1 from level | 10 base<br>2 from ability scores<br>1 from level | 12 base<br>-1 from ability scores<br>1 from level |

### attacks

- Outmaneuver (quick action) (with 0 command points only): +CHA vs. MD
- Melee: +4 vs. AC, 1d6+3 damage; 1 damage on a miss
- Ranged: +2 vs. AC, 1d6+1 damage; 1 damage on a miss

## class features

- fight from the front
  - gain 1d3 command points on melee hit
- weigh the odds
  - standard: gain 1d4 command points

## abilities

KEY:

| ED | CP | 1/D | 1/B | IR | RC#+ |
|----|----|----|----|----|----|
| escalation die | command point | 1 per day | 1 per battle | icon roll | recharge #+ |

## talents (3 at level 1)

- **armor skills**
  - no penalty from heavy armor
  - feats

    > ~~A: enemy miss 1 or 2 gain 1CP~~

    > ~~C: 1/D: when target of AC attack, take half damage~~

    > ~~E: 1/D: 0-icon bonus to AC, up to ED~~

- **combat maneuver** (chosen: WIP)
  - choose fighter maneuver your level or lower; use it like a fighter; can switch for different each level up
  - pick one of:
    - ~~defensive fighting - be extra tanky (nat 16+ or even, +2 AC till end of next turn), moderately useful, would bump to 19 AC, 21 with into the fray (for first round)~~
    - carve an opening - be extra pokey (nat odd, crit expands by +1 until next crit), neat
    - ~~deadly assault - be extra pokey (nat even, reroll any 1s), seems limited usefulness with a single damage die~~
  
  - feats

    > ~~A: gain Adv for maneuver~~

    > ~~C: choose 2nd maneuver~~

    > ~~E: gain C and E tier feats for both maneuvers~~

- **into the fray**
  - before init, roll d4, number of allies equal to roll gain +4 init (for current battle), or +2 AC (until end of first round)
  - feats

    > ~~A: you also gain bonus~~

    > ~~C: +2 bonus applies to PD and MD~~

    > ~~E: allies now gain both init and def bonuses~~

## commands (total of 4 commands+tactics at level 1)

- **(1 CP) save now**
  - Target: nearby ally
  - Effect: can roll a save
  - feats

    > ~~A: more points to roll more~~

    > ~~C: gain 1 CP on fail~~

    > ~~E: target heals 3d10+CHA*3 (holy crap)~~

- **(2 CP) try again**
  - Target: nearby ally that rolled attack
  - Effect: can reroll attack but must take
  - feats

    > A: roll bonus equal to CHA if escalation 3+

    > ~~C: meh~~

    > ~~E: use 3 CP to give target extra attack instead~~

- **(4 CP) you set them up, I finish**
  - Target: nearby ally that hits enemy in sight
  - Effect: add CHA to dmg dealt by ally, +2 attk your next turn on same enemy
  - feats

    > A: ally gets dmg bonus on other attacks on same enemy this round

## tactics (total of 4 commands+tactics at level 1)

- **(per round) outmaneuver**
  - Target: nearby highest MD enemy
  - Effect: at-will attack, close-quarters, CHA vs MD to hit, gain 1 command point (no damage)
  - Special: can only use when have 0 CP
  - feats

    > ~~A: target highest MD engaged rather than nearest~~

    > ~~C: nat even = 2 CP~~

    > ~~E: EDd10 psychic~~

### racial power

- **(on hp 0) never say die**
  - Trigger: Whenever you drop to 0 hp or below, roll a normal save if you have a recovery available.
  - Effect: On an 11+, instead of falling unconscious, you stay on your feet and can heal using a recovery. Add the recovery hit points to 0 hp to determine your hp total.
  - feats
     
    > ~~C: If you roll a 16+ on your never-say-die save, you gain an additional standard action during your next turn.~~

### gear

- 10gp
- (heavy armor) plating and other heavy bits assimilated into its frame, a bit beat up, but it flies true
- some light throwing spears
- 1d6 hand-axe
- shield
- ball bearings, always need those
- a small pot of tar
- stuff - WIP

## history/notes

#### Writing prompts backstory

- realize just shoplifted: gaslight store owner
- last time peed bed: offense at doubt over waste disposal system quality
- domestic violence: ask if perpetrator needs assistance to perform discipline (bit dark)
- who prayed to as child: what childhood, also religion is a social construct of organic beings
- feelings on mud: keep it out of joints to reduce maintenance, but useful tool against organics (traps and such)

Sessions

- [2020-06-03](https://gitlab.com/modle13/13th-age-characters/-/issues/2)
- [2020-06-24](https://gitlab.com/modle13/13th-age-characters/-/issues/1)
